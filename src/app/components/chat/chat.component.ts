import { Component, OnInit } from '@angular/core';

// Servicio
import { ChatService } from './../../providers/chat.service';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styles: []
})
export class ChatComponent implements OnInit {

  mensaje = '';
  elemento: any;

  constructor( public _cs: ChatService) {


    // Nos suscribimos al observable
    this._cs.cargarMensajes()
            .subscribe( () => {
                // Pone el foco en el ultimo mensaje recibido
                setTimeout( () => {
                    this.elemento.scrollTop = this.elemento.scrollHight;
                }, 40);

            } );


  }

  ngOnInit() {
      this.elemento = document.getElementById('app-mensajes');
  }

  enviar_mensaje() {
    // Validamos si existe un mensaje antes de ejecutar la función

    if (this.mensaje.length === 0) {
      return;
    } else {
      this._cs.insertarMensaje(this.mensaje)
      // Envio correcto
        .then(() => { console.log('Mensaje enviado'); this.mensaje = ''; })
        // Error al enviar
        .catch( (err) => console.error('Error al enviar el mensaje', err ) );
    }
    console.log(this.mensaje);
  }

}
