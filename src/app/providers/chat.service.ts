import { Injectable } from '@angular/core';

// Collections in angularfire2
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

// Interface
import { Mensaje } from '../interface/mensaje';

// Login
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@Injectable()
export class ChatService {
  // lee una colección
  private itemsCollection: AngularFirestoreCollection<Mensaje>;

  public chats: Mensaje[] = []; // Nombre de nuestra coleccion en firebase

  public usuario: any = {};

  constructor(
        private afs: AngularFirestore,
        public afAuth: AngularFireAuth) {
            // Escuchamos cualquier cambio en el estado de la autenticación
            this.afAuth.authState.subscribe(user => {
                console.log('estado del usuario:', user);

                if (!user) {
                    return;
                }
                 this.usuario.nombre = user.displayName;
                 this.usuario.uid = user.uid;
            });
        }

    login( proveedor: string) {

        // Twitter o Google
        if (proveedor === 'google') {
            this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
        } else {
            this.afAuth.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider());
        }

    }
    logout() {
        this.usuario = {};
        this.afAuth.auth.signOut();
    }


    cargarMensajes () {
        // Retorna un observable que avisa si se cargaron o no. consulta y ordenamiento de los mensajes
        this.itemsCollection = this.afs.collection<Mensaje>('chats', ref => ref.orderBy('fecha', 'desc')
                                                                                .limit(5) ); // ultimos 5 mensajes
        // Pendiente de todos los cambios que suceden en el nodo mensajesChats
        // Retorna un observable, transforma lo que recibe y lo regresa
        return this.itemsCollection.valueChanges() // map se ejecuta cuando nos suscribimos
                                    .map( (mensajes: Mensaje[]) => {
                                        // imprimimos el json de los mensajes
                                        console.log(mensajes);
                                        // guardamos los mensajes
                                        this.chats = [];
                                        // invertimos los mensajes para que se muestren correctamente
                                        for ( const mensaje of mensajes ) {
                                            this.chats.unshift( mensaje );
                                        }
                                        return this.chats;
                                        // this.chats = mensajes;

                                    });
    }

    // Recibe el texto del mensaje
    insertarMensaje ( texto: string ) {
        // Mandamos la esructura completa a insertar
        // TODO: falta el uid del usuario
        const msg: Mensaje = {
            nombre: this.usuario.nombre,
            mensaje: texto,
            fecha: new Date().getTime(),
            uid: this.usuario.uid
        };

        // Para hacer la inserción en firebase- retorna una promesa
        return this.itemsCollection.add(msg);
    }

}
