// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBl47suaHUzCGFH9Vzn5-LujdvRsKzymAY',
    authDomain: 'firechat-45ec9.firebaseapp.com',
    databaseURL: 'https://firechat-45ec9.firebaseio.com',
    projectId: 'firechat-45ec9',
    storageBucket: 'firechat-45ec9.appspot.com',
    messagingSenderId: '679259330818'
  }
};
